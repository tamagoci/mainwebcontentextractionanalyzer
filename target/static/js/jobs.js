function getwork(){
   $.ajax({
        url:"/apic",
        method:"GET",
    }).done(wrk);
}


function wrk(jobs){
    jobs.map(add);
}

hashCode = function(s){
      return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
}


function add(newobj){
    var b = $("#template").clone();
    newobj.id = hashCode(newobj.url)
    b[0].id = "id"+newobj.id;
    b[0].style = "";
    b.appendTo("#results");


    var tmp = "#id"+newobj.id+" #boiler .panel-title";
    var t = $(tmp).text("Boiler "+newobj.statistics.boiler.time+"s ");

    var tmp = "#id"+newobj.id+" #boiler .panel-body p";
    var t = $(tmp).text(newobj.extracted.boiler );

    $(tmp).parent().parent().hide();


    var tmp = "#id"+newobj.id+" #dragnet .panel-title";
    var t = $(tmp).text("Dragnet "+newobj.statistics.boiler.time+"s ");

    var tmp = "#id"+newobj.id+" #dragnet .panel-body p";
    var t = $(tmp).text(newobj.extracted.dragnet );


    $(tmp).parent().parent().hide();


    var tmp = "#id"+newobj.id+" #newspaper .panel-title";
    var t = $(tmp).text("Newspaper "+newobj.statistics.newspaper.time+"s");

    var tmp = "#id"+newobj.id+" #newspaper .panel-body p";
    var t = $(tmp).text(newobj.extracted.newspaper );

    $(tmp).parent().parent().hide();

    var tmp = "#id"+newobj.id+" #origigi .panel-body p";
    var t = $(tmp).text(newobj.extracted.manual);

    $(tmp).parent().parent().hide();


    for (var propn in newobj.statistics){
        var jg = newobj.statistics[propn];
        var tmp2 = $("#id"+newobj.id+" #"+ propn +" .panel-title");
        tmp2.text(tmp2.text()+" P:"+jg.p+" R:"+jg.r+" F1:"+jg.f1);

    }


    v = $("#id"+newobj.id+" a");
    v.text(newobj.url);
    v[0].href = newobj.url;

}
