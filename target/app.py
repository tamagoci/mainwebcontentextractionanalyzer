#!/usr/bin/env python
import time
import requests
import commands
from json import dumps
from dragnet import content_extractor, content_comments_extractor
from IPython import embed
import hashlib
from splitter import *

from bottle import *
from boilerpipe.extract import Extractor

import sqlite3


app = Bottle()

JOBS = []

dbs = sqlite3.connect("analyzer.sqlite")

TOOLS = {
        0:"boiler",
        1:"dragnet",
        2:"newspaper"
        }

dbs.text_factory = str

@app.route("/")
def rdrkt():
    return redirect("/index.html")

def insert_manual(data):
    cur = dbs.cursor()
    cur.execute(
            '''
            INSERT INTO mExtracted
            VALUES
            (?,?,?,?,?);
            ''',data)
    dbs.commit()
    return data


def insert_tool(db, url, tool, rez, tim):
    cur = dbs.cursor()

    cur.execute("""INSERT INTO aExtracted (url, tool_id, content, time) values
    (?,?,?,?)""",(url,tool,rez, tim))

    dbs.commit()

@app.route("/api",method="POST")
def jobcreate():
    turl = request.forms.get("url")
    tcontent= request.forms.get("content").decode("utf-8",'ignore')
    ttype= request.forms.get("type")
    tlang= request.forms.get("language")
    tsu= request.forms.get("shortUrl")

    if not tsu:
        tsu = "/".join(turl.split("/")[:3])

    insert_manual((turl, tcontent,ttype,tlang,tsu),)

    boil, boil_time = extract_with_boiler(turl)
    insert_tool(dbs,turl,0,boil,boil_time)

    drag, drag_time = extract_with_dragnet(turl)
    insert_tool(dbs,turl,1,drag, drag_time)

    nwpap, nwpap_time = extract_with_newspaper(turl)
    insert_tool(dbs,turl,2,nwpap, nwpap_time)

    oc = set(map_md5(split_sentences(tcontent)))
    bc = set(map_md5(split_sentences(boil)))
    dc = set(map_md5(split_sentences(drag)))
    nc = set(map_md5(split_sentences(nwpap)))

    insert_statistics(dbs,turl,0, *calculate_statistics(oc,bc))
    insert_statistics(dbs,turl,1, *calculate_statistics(oc,dc))
    insert_statistics(dbs,turl,2, *calculate_statistics(oc,nc))

    return redirect("/index.html")


def calculate_statistics(orig_set, extracted_set):
    if len(extracted_set) == 0:
        return 0,0,0
    Pb = len(orig_set.intersection(extracted_set))/float(len(extracted_set))
    Rb = len(orig_set.intersection(extracted_set))/float(len(orig_set))

    F1b = 2 * (Pb * Rb)/float(Pb + Rb)


    return Pb, Rb, F1b



def insert_statistics(db, url, tool_id, precision, recall, f1):
    cur = db.cursor()
    cur.execute("""
            INSERT INTO statistics VALUES (?, ?, ?, ?, ?)
            """,(url, tool_id, precision, recall, f1))
    db.commit()



@app.route("/apic")
def export():
    Jsonic = []
    cur = dbs.cursor()
    cur.execute("select url,language,type, content from mExtracted")
    rez = cur.fetchall()
    for i,lang,tip,cont in rez:
        url = i
        cur.execute("select * from aExtracted where url=?",(url,))
        rr = cur.fetchall()

        cur.execute("select * from statistics where url=?",(url,))
        statrr = cur.fetchall()


        tmp = {}
        tmp = {"statistics":{"boiler":{},"newspaper":{},"dragnet":{}}}
        tmp["url"] = url

        for i in statrr:
            uu,tool,p,r,f1 = i
            p = round(p,3)
            r = round(r,3)
            f1 = round(f1,3)
            tmp["statistics"][TOOLS[int(tool)]].update({"p":p,"r":r,"f1":f1})

        tmp["extracted"] = {
                "manual":cont}

        for v in rr:
            tmp['extracted'][TOOLS[v[1]]] = v[2]#content
            tmp["statistics"][TOOLS[v[1]]]["time"] = round(v[3],3)#time

        tmp["language"]=lang
        tmp["type"] = tip

        Jsonic.append(tmp)



    response.content_type = 'application/json'
    return dumps(Jsonic)



def extract_with_boiler(your_url):
    t = time.time()
    try:
        extractor = Extractor(extractor='ArticleExtractor', url=your_url)
        extracted_text = extractor.getText()
        return extracted_text,round(time.time()-t,3)
    except Exception,e:
        print "Error extracting %s %s with %s"%(your_url,e, "boiler")
        return "",time.time()-t

def extract_with_dragnet(url):
    t = time.time()
    try:
        r = requests.get(url)
        content = content_extractor.analyze(r.content)
        return content.decode("utf-8"), time.time()-t
    except Exception,e:
        print "Error extracting %s %s with %s"%(url,e, "dragnet")
        return "",time.time()-t
    return "", round(time.time()-t)

def extract_with_newspaper(url):
    t = time.time()
    try:
        content = commands.getoutput("python3 app3.py '%s'"%url).decode("string_escape")
        return content.encode("utf-8",'ignore'), round(time.time()-t,3)
    except Exception,e:
        print "Error extracting %s with %s"%(url, "newspaper")
        return "",time.time()-t

@app.route("/apic/success")
def success():
    cur = dbs.cursor()
    manual = float(cur.execute("select count(*) from mExtracted;").fetchall()[0][0])
    boil = cur.execute("select count(*) from statistics where f1 !=0 and tool_id=?",(0,)).fetchall()[0][0]
    drag = cur.execute("select count(*) from statistics where f1 !=0 and tool_id=?",(1,)).fetchall()[0][0]
    news = cur.execute("select count(*) from statistics where f1 !=0 and tool_id=?",(2,)).fetchall()[0][0]
    response.content_type = 'application/json'

    Jsonic = {"boil":(boil/manual)*100,
            "drag":(drag/manual)*100,
            "news":(news/manual)*100}

    return dumps(Jsonic)


@app.route("/apic/success/t/<typ>")
def successtype(typ):
    cur = dbs.cursor()
    manual = float(cur.execute("select count(*) from mExtracted where type=?;",(typ,)).fetchall()[0][0])
    boil = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and type=?",(0,typ)).fetchall()[0][0]
    drag = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and type=?",(1,typ)).fetchall()[0][0]
    news = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and type=?",(2,typ)).fetchall()[0][0]
    response.content_type = 'application/json'

    Jsonic = {"boil":(boil/manual)*100,
            "drag":(drag/manual)*100,
            "news":(news/manual)*100}

    return dumps(Jsonic)

@app.route("/apic/success/l/<typ>")
def successlang(typ):
    cur = dbs.cursor()
    manual = float(cur.execute("select count(*) from mExtracted where language=?;",(typ,)).fetchall()[0][0])
    boil = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and language=?",(0,typ)).fetchall()[0][0]
    drag = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and language=?",(1,typ)).fetchall()[0][0]
    news = cur.execute("select count(*) from statistics natural join mExtracted where f1 !=0 and tool_id=? and language=?",(2,typ)).fetchall()[0][0]
    response.content_type = 'application/json'

    Jsonic = {"boil":(boil/manual)*100,
            "drag":(drag/manual)*100,
            "news":(news/manual)*100}

    return dumps(Jsonic)



@app.route("/apic/precision")
def precision():
    cur = dbs.cursor()
    boil = cur.execute("select precision from statistics where f1 !=0 and tool_id=?",(0,)).fetchall()
    drag = cur.execute("select precision from statistics where f1 !=0 and tool_id=?",(1,)).fetchall()
    news = cur.execute("select precision from statistics where f1 !=0 and tool_id=?",(2,)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    Jsonic["news"] = sum([i[0] for i in news])/float(len(news))

    return dumps(Jsonic)

@app.route("/apic/recall")
def recall():
    cur = dbs.cursor()
    boil = cur.execute("select recall from statistics where f1 !=0 and tool_id=?",(0,)).fetchall()
    drag = cur.execute("select recall from statistics where f1 !=0 and tool_id=?",(1,)).fetchall()
    news = cur.execute("select recall from statistics where f1 !=0 and tool_id=?",(2,)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    Jsonic["news"] = sum([i[0] for i in news])/float(len(news))

    return dumps(Jsonic)

@app.route("/apic/f1")
def f1():
    cur = dbs.cursor()
    boil = cur.execute("select f1 from statistics where f1 !=0 and tool_id=?",(0,)).fetchall()
    drag = cur.execute("select f1 from statistics where f1 !=0 and tool_id=?",(1,)).fetchall()
    news = cur.execute("select f1 from statistics where f1 !=0 and tool_id=?",(2,)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    Jsonic["news"] = sum([i[0] for i in news])/float(len(news))

    return dumps(Jsonic)

@app.route("/apic/time")
def timer():
    cur = dbs.cursor()
    boil = cur.execute("select time from aExtracted where tool_id=?",(0,)).fetchall()
    drag = cur.execute("select time from aExtracted where tool_id=?",(1,)).fetchall()
    news = cur.execute("select time from aExtracted where tool_id=?",(2,)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    Jsonic["news"] = sum([i[0] for i in news])/float(len(news))

    return dumps(Jsonic)


@app.route("/apic/reprocess", method="POST")
def allproc():
    cur = dbs.cursor()
    urls = request.forms.getall("opti")
    for i in urls:
        url = i
        cur.execute("DELETE FROM aExtracted where url=?",(i,));
        dbs.commit()
        r,t =  extract_with_boiler(url)
	insert_tool(dbs,url,0,r,t)
        r,t =  extract_with_dragnet(url)
	insert_tool(dbs,url,1,r,t)
        r,t =  extract_with_newspaper(url)
	insert_tool(dbs,url,2,r,t)

    return redirect("/index.html")
    

@app.route("/apic/all")
def allent():
    cur = dbs.cursor()
    alle = cur.execute("select url from mExtracted").fetchall()

    response.content_type = 'application/json'
    return dumps([i[0] for i in alle])


@app.route("/apic/t/<tag>", method="GET")
def subject(tag):
    cur = dbs.cursor()
    boil = cur.execute('select f1 from mExtracted natural join statistics where type=? and tool_id=? and f1>0;',(tag,0)).fetchall()
    drag = cur.execute('select f1 from mExtracted natural join statistics where type=? and tool_id=? and f1>0;',(tag,1)).fetchall()
    news = cur.execute('select f1 from mExtracted natural join statistics where type=? and tool_id=? and f1>0;',(tag,2)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    if len(boil) > 0:
        Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    else:
        Jsonic["boil"] = 0.0

    if len(drag) > 0:
        Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    else:
        Jsonic["drag"] = 0.0

    if len(news) > 0:
        Jsonic["news"] = sum([i[0] for i in news])/float(len(news))
    else:
        Jsonic["news"] = 0.0

    return dumps(Jsonic)





@app.route("/apic/l/<lang>")
def server_static(lang):
    tag = lang
    cur = dbs.cursor()
    boil = cur.execute('select f1 from mExtracted natural join statistics where language=? and tool_id=? and f1>0;',(tag,0)).fetchall()
    drag = cur.execute('select f1 from mExtracted natural join statistics where language=? and tool_id=? and f1>0;',(tag,1)).fetchall()
    news = cur.execute('select f1 from mExtracted natural join statistics where language=? and tool_id=? and f1>0;',(tag,2)).fetchall()
    response.content_type = 'application/json'

    Jsonic = {}
    if len(boil) > 0:
        Jsonic["boil"] = sum([i[0] for i in boil])/float(len(boil))
    else:
        Jsonic["boil"] = 0.0

    if len(drag) > 0:
        Jsonic["drag"] = sum([i[0] for i in drag])/float(len(drag))
    else:
        Jsonic["drag"] = 0.0

    if len(news) > 0:
        Jsonic["news"] = sum([i[0] for i in news])/float(len(news))
    else:
        Jsonic["news"] = 0.0

    return dumps(Jsonic)



@app.route("/<filepath:path>")
def server_static(filepath):
    return static_file(filepath, root='./static/')


if __name__=="__main__":
    app.run(host="0.0.0.0", port="8009", debug=True)
