import sys
import re

import hashlib



def map_md5(splitted_txt):
    '''
    Returns:
        original_sentence, md5sum
    '''
    return map(lambda x: 
            hashlib.md5(x.encode("utf-8")).hexdigest(),
            splitted_txt)


def split_sentences(all_text):
    '''
    Returns list splitted sentences by ". ","! ","? "
    '''
    clean_ws_text = re.sub(r'\s+', " ", all_text)

    splitter = [clean_ws_text,]
    for i in [". ","! ","? "]:
        tmp = []
        for v in splitter:
            tmp.extend(v.split(i))

        splitter = tmp

    return [i for i in splitter
            if i]


if __name__ == "__main__":
    text = sys.argv[1]
    for g,v in map_md5(split_sentences(text)):
        print "-"*30
        print v,g

