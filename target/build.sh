#!/bin/sh
git clone https://github.com/misja/python-boilerpipe.git
git clone https://github.com/seomoz/dragnet.git

#boilerpipe
cd python-boilerpipe/
pip install -r requirements.txt
python setup.py install
cd ..


#dragnet
cd dragnet
pip install -r requirements.txt
make install
make test
cd ..


mv .vimrc .tmux.conf /root/
