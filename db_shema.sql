CREATE DATABASE analyzer;

CREATE TABLE mExtracted (
    url         TEXT PRIMARY KEY,
    content     TEXT NOT NULL,
    type        TEXT NOT NULL,
    language    TEXT NOT NULL,
    shortUrl    TEXT NOT NULL
);

CREATE TABLE tools (
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    name    TEXT NOT NULL
);

CREATE TABLE aExtracted (
    url     TEXT,
    tool_id INTEGER,
    content TEXT NOT NULL,
    time    REAL NOT NULL,
    PRIMARY KEY (url, tool_id),
    FOREIGN KEY (tool_id) REFERENCES tools(id),
    FOREIGN KEY (url) REFERENCES mExtracted(url)
);

CREATE TABLE statistics (
    url         TEXT,
    tool_id     INTEGER,
    precision   REAL NOT NULL,
    recall      REAL NOT NULL,
    f1          REAL NOT NULL,
    PRIMARY KEY (url, tool_id),
    FOREIGN KEY (tool_id) REFERENCES tools(id),
    FOREIGN KEY (url) REFERENCES mExtracted(url)
);


