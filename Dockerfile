FROM ubuntu:16.04

WORKDIR /app

ADD target /app

RUN apt-get -qq update

RUN apt-get install -y python-virtualenv python-chardet git build-essential python-jpype python-pip python-lxml python-numpy cython cython-dbg libxml++2.6-dev libxml2-dev libxslt1-dev python-dev libxml2-dev libxslt-dev  libjpeg-dev zlib1g-dev libpng12-dev python3-dev python3-pip

RUN apt-get install -y vim python-requests tmux ipython

RUN pip install bottle 

RUN pip3 install newspaper3k

RUN ./build.sh

EXPOSE 8009

ENV NAME DBDip

CMD ["python", "app.py"]
